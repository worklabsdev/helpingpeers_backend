const { authHandler, errorHandler } = require("../responseHandler");

const { authCaseTypes } = require("./constants");

const apiRef = "authMiddleware";
