const jwt = require("jsonwebtoken");

// ///   Generate JWT
exports.generateJWT = (obj, jwtValidity = appConfig.auth.jwtValidity) => jwt.sign(
	obj,
	appConfig.auth.jwtKey,
	{
		expiresIn: jwtValidity, // '24h' // expires in 24 hours
	},
);

// ///       Decode JWT
exports.decodeJWT = (token) => jwt.verify(token, appConfig.auth.jwtKey, (err, decoded) => {
	if (err) return { success: false };
	return { success: true, decoded };
});
