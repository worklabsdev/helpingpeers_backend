const errorConfig = appConfig.errorSettings;

const { statusCodes, successCodes, globalMessages } = require("../config/properties/constants");
const { authCaseTypes } = require("./auth/constants");

// let logger = require("../packages/logging");

// ///       Error Handler
exports.errorHandler = (response, error, data = {}, odata) => {
    // logger.fatal("fatal", error);
    console.error("~~~~~~~~~~~~~~~~~~~ERROR~~~~~~~~~~~~~~~~~~~", error, odata);
    console.log("~~~~~~~~~~~~~~~~~~~ERROR~~~~~~~~~~~~~~~~~~~");

    const success = successCodes.error;

    let message = response.trans(globalMessages.responseMessages.ErrorMsg);
    if (errorConfig.showError) message = error.message;

    return response.status(statusCodes.error).json({
        status: success,
        message,
        data,
    });
};

// ///       Error Handler View
exports.errorHandlerView = (response, error, data = {}) => {
    console.error("~~~~~~~~~~~~~~~~~~~ERROR~~~~~~~~~~~~~~~~~~~", error, data);
    console.log("~~~~~~~~~~~~~~~~~~~ERROR~~~~~~~~~~~~~~~~~~~");

    let message = response.trans(globalMessages.responseMessages.ErrorMsg);
    if (errorConfig.showError) message = error.message;

    return response.render("Common/error", { message, appName: appConfig.appName });
};


// ///       Success Handler
exports.successHandler = (response, msg = null, data = {}) => {
    const { success } = successCodes;
    const message = response.trans(msg || globalMessages.responseMessages.SuccessMsg);

    return response.status(statusCodes.success).json({
        status: success,
        message,
        data,
    });
};
// ///       Success Handler View
exports.successHandlerView = (response, msg = null) => {
    const message = response.trans(msg || globalMessages.responseMessages.SuccessMsg);

    return response.render("Common/success", { message, appName: appConfig.appName });
};

// ///       Created Handler
exports.createdHandler = (response, msg = null, data = {}) => {
    const success = successCodes.created;
    const message = response.trans(msg || globalMessages.responseMessages.SuccessMsg);

    return response.status(statusCodes.created).json({
        status: success,
        message,
        data,
    });
};

// ///       Validation Handler
exports.validationHandler = (response, msg = null, data = {}) => {
    const success = successCodes.validationFailed;
    const message = msg || globalMessages.responseMessages.SuccessMsg;
    // response.trans(msg || globalMessages.responseMessages.successMsg);

    return response.status(statusCodes.validationFailed).json({
        status: success,
        message,
        data,
    });
};

// ///       Failed Action Handler
exports.failedActionHandler = (response, msg = "Something, went wrong please try again", data = {}, settings = { trans: true }) => {
    const success = successCodes.actionFailed;

    let message = msg;
    if (settings.trans) message = response.trans(msg);

    return response.status(statusCodes.actionFailed).json({
        status: success,
        message,
        data,
    });
};

// ///       Auth Handler
exports.authHandler = (response, type = 2) => {
    const success = successCodes.authFailed;

    let message;

    switch (type) {
        case authCaseTypes.NO_TOKEN:
            message = response.trans(globalMessages.responseMessages.AuthHeaderMissing);
            break;
        case authCaseTypes.NOT_VALID_TOKEN:
            message = response.trans(globalMessages.responseMessages.AuthFailedMsg);
            break;
        case authCaseTypes.ACCOUNT_SUSPENDED:
            message = response.trans(globalMessages.responseMessages.AccountSuspended);
            break;
        case 4:
            message = response.trans(globalMessages.responseMessages.AccountNotFound);
            break;
        case 5:
            message = response.trans(globalMessages.responseMessages.OtpVerificationRequired);
            break;
        default:
            message = response.trans(globalMessages.responseMessages.AuthFailedMsg);
            break;
    }

    return response.status(statusCodes.authFailed).json({
        status: success,
        message,
    });
};

// ///		Handle 404
exports.pageNotFound = (response) => {
    const success = successCodes.pageNotFound;
    const message = response.trans(globalMessages.responseMessages.PageNotFound);

    return response.status(statusCodes.pageNotFound).json({
        status: success,
        message,
    });
};
