const randomstring = require("randomstring");
const moment = require("moment");
const slug = require("slug");
const uuidv1 = require("uuid/v1");
const underscore = require("underscore");
const mongoose = require("mongoose");
const shortid = require("shortid");

//	invite Code Generate
const inviteCodeGenerator = () => shortid.generate();
exports.inviteCodeGenerator = inviteCodeGenerator;

const generateFutureDt = (minutes) => moment().utc().add(minutes, "minutes");
exports.generateFutureDt = generateFutureDt;

const currentUTC = () => moment().utc();
exports.currentUTC = currentUTC;

const futureDtCheck = (dt) => moment.utc(dt).isAfter(currentUTC());
exports.futureDtCheck = futureDtCheck;

const generateRandStr = (len) => randomstring.generate(len);
exports.generateRandStr = generateRandStr;

const utcConvertor = (dt, timezone, format = "YYYY-MM-DD hh:mm:ss") => moment.tz(dt, format, timezone).utc().format(format);
exports.utcConvertor = utcConvertor;

const generateOTP = (length = 4) => {
    const { otpValidityMinutes } = appConfig.sms;

    const otp = 4444;

    // let otp = randomstring.generate({
    //   	length: length,
    //   	charset: "numeric"
    // });

    const otpValidity = generateFutureDt(otpValidityMinutes);

    return { otp, otpValidity };
};
exports.generateOTP = generateOTP;

//	Slug Creator
const slugCreator = (text) => {
    const slugGen = slug(text);
    const randGen = generateRandStr(20);

    return `${slugGen}-${randGen}`;
};
exports.slugCreator = slugCreator;

//	Generate Unique Code
const generateUniqueCode = () => uuidv1();
exports.generateUniqueCode = generateUniqueCode;

//	Unique Array Convertor
const uniqueArrayConvertorFunc = (arr) => underscore.uniq(arr);
exports.uniqueArrayConvertorFunc = uniqueArrayConvertorFunc;

//	Round Decomels
const roundToXDecimals = (value, decimals = 2) => Number(`${Math.round(`${value}e${decimals}`)}e-${decimals}`);
exports.roundToXDecimals = roundToXDecimals;

//  Generate New Object
const generateObjectId = () => mongoose.Types.ObjectId();
exports.generateObjectId = generateObjectId;
