var studentModel = require('./app/models/student');
var jwt = require('jsonwebtoken');
module.exports.isAuthorised = (req,res,next)=>{
  if(!req.headers.authorization) {
    return res.status(403).json({error : 'No Credentials sent'});
  }
  else {
    studentModel.find({token : req.headers.authorization}).exec(err,data)=>{
      if(err) return next(err);
      console.log("you got the user logged in ");
      if(data){
      return next();
      }
      else {
        res.status(403).send("no user found invalid token");
      }

    }
  }
}
