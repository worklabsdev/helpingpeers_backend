const Joi = require("@hapi/joi");

const apiRef = "StudentAuth";

const { joiValidate, joiValidateView } = require("../../../packages/joi");

//	Register
exports.register = async (request, response, next) => {
    request.apiRef = apiRef;
    request.apiRefFull = `${apiRef}/register`;

    const schema = Joi.object({
        firstname: Joi.string().required(),
        class: Joi.string().required(),
        age: Joi.alternatives().try(Joi.string(), Joi.number()).required(),
        email: Joi.string().email().required(),
        password: Joi.string().required(),
    });

    const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, {
        requestId: request.requestId
    });
    if (valCheck === true) next();
};

//	Confirm Account
exports.confirmAccount = async (request, response, next) => {
    request.apiRef = apiRef;
    request.apiRefFull = `${apiRef}/confirmAccount`;

    const schema = Joi.object().keys({
        token: Joi.string().required()
    });

    request.body = {
        ...request.params
    };

    const valCheck = await joiValidateView(request.apiRefFull, response, schema, request.body, {
        requestId: request.requestId
    });
    if (valCheck === true) next();
};

//	Check Email
exports.checkStudentEmail = async (request, response, next) => {
    request.apiRef = apiRef;
    request.apiRefFull = `${apiRef}/checkStudentEmail`;

    const schema = Joi.object().keys({
        email: Joi.string().email().required()
    });

    request.body = {
        ...request.params
    };

    const valCheck = await joiValidate(request.apiRefFull, response, schema, request.body, {
        requestId: request.requestId
    });
    if (valCheck === true) next();
};
