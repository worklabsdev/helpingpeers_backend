
const { studentRegisterMailHelperFunc } = require("../../../helpers/mails");

const registerationHelperModuleFunc = (student, odata) => {

    //  Mail
    studentRegisterMailHelperFunc(
        student,
        odata
    );
    //  Mail

};
exports.registerationHelperModuleFunc = registerationHelperModuleFunc;