const responseHandler = require("../../../utils/responseHandler");
const { generateObjectId, generateRandStr } = require("../../../utils/commonFuns");
const { generateJWT } = require("../../../utils/auth/auth");

const { globalMessages } = require("../../../config/properties/constants");

//const { uploadFile } = require(`../../../packages/upload/${appConfig.upload.name}`);

const studentSer = require("../../../services/studentSer");

const helpers = require("./helpers");

//  Register
const register = async (request, response) => {
    try {
        //  Unique Check
        let uniqueCheck = await studentSer.studentGetSer({ email: request.body.email, isDeleted: false });
        if (uniqueCheck) {
            return responseHandler.failedActionHandler(response, globalMessages.responseMessages.EmailAlreadyRegMsg);
        }
        //  Unique Check

        request.body._id = generateObjectId();

        //  JWT Token
        let userdata = {
            _id: request.body._id,
            firstname: request.body.firstname,
            email: request.body.email
        };
        request.body.token = generateJWT(userdata);
        //  JWT Token

        request.body.token = generateRandStr(100);

        let student = await studentSer.studentCreateSer(request.body);

        //  Student Register Helper Func
        helpers.registerationHelperModuleFunc(
            JSON.parse(JSON.stringify(student)),
            {
                apiRefFull: request.apiRefFull,
                requestId: request.requestId
            }
        );
        //  Student Register Helper Func

        return responseHandler.successHandler(response, null, {
            token: request.body.token,
            userdata
        });
    }
    catch (error) {
        return responseHandler.errorHandler(response, error, { data: request.body }, {
            apiRefFull: request.apiRefFull,
            requestId: request.requestId
        });
    }
};
exports.register = register;

//	Confirm Account
const confirmAccount = async (request, response) => {
    try {
        let user = await studentSer.studentGetSer({
            token: request.body.token,
            isDeleted: false
        });
        if (!user || user.emailVerified) {
            return responseHandler.errorHandlerView(response, { message: globalMessages.responseMessages.LinkExpired });
        }

        //	Update User
        await studentSer.studentUpdateSer(
            { _id: user._id },
            {
                $set: {
                    token: "",
                    emailVerified: true,
                    updatedAt: Date.now()
                }
            }
        )
        //	Update User

        return responseHandler.successHandlerView(response, globalMessages.responseMessages.AccountVerifiedSuccessMsg, { appName: appConfig.appName });
    } catch (error) {
        return responseHandler.errorHandlerView(response, error);
    }
};
exports.confirmAccount = confirmAccount;

//  Check Email
const checkStudentEmail = async (request, response) => {
    try {

        let student = await studentSer.studentGetSer({ email: request.body.email, isDeleted: false }, { _id: 1 });

        let message = null;
        let unique = true;
        if (student) {
            message = globalMessages.responseMessages.EmailAlreadyRegMsg;
            unique = false;
        }

        return responseHandler.successHandler(response, message, {
            unique
        });

    } catch (error) {
        return responseHandler.errorHandler(response, error, {}, {
            apiRefFull: request.apiRefFull,
            requestId: request.requestId
        });
    }
};
exports.checkStudentEmail = checkStudentEmail;