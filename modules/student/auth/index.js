const router = express.Router();

//const { buyerVerifyMiddleware } = require("../../../../utils/auth/middlewares");

const validators = require("./validators");
const controllers = require("./controllers");

router.post("/register", validators.register, controllers.register);

router.get("/verifyemail/:token", validators.confirmAccount, controllers.confirmAccount);

router.get('/checkStudentEmail/:email', validators.checkStudentEmail, controllers.checkStudentEmail);

module.exports = router;
