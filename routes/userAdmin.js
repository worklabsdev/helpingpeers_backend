var express = require("express");
var router = express.Router();
var adminController = require("../app/controllers/admin.controller");
var adminModel = require("../app/models/admin");

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

// Post requests
router.post("/login", adminController.authenticate);
router.post("/register", adminController.register);
// Get requests
router.get("/students", adminController.getAllStudents);
router.get("/teachers", adminController.getAllTeachers);
router.get("/teacher/:name/:email", adminController.getSingleTeacher);
router.get("/student/:id", adminController.getSingleStudent);
router.get("/getalladminqueries", adminController.getAllAdminQueries);
router.get("/getsingleadminquery/:id", adminController.getSingleAdminQuery);
router.get("/getpendingteachers", adminController.getPendingTeachers);
router.get("/getpendingteacherscount", adminController.getPendingTeachersCount);
router.get("/getteachercount", adminController.getTeacherCount);
router.get("/getstudentcount", adminController.getStudentCount);
//  Put requests
router.put("/teacherdegreeapprove/:id", adminController.teacherDegreeApprove);
router.put("/adminreply/:id", adminController.adminReply);
router.put("/enabledisablestudent/:id", adminController.enableDisableStudent);
router.put("/enabledisableteacher/:id", adminController.enableDisableTeacher);

module.exports = router;
