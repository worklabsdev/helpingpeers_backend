var express = require('express');
var router = express.Router();
// var AuthRouter = express.Router();
var studentController = require('../app/controllers/student.controller');
var studentModel = require('../app/models/student');
// var auth = require('../auth');

/* GET users listing. */
// router.get('/', studentController.verifyToken);
// router.post('/',studentController.verifyToken);

function AuthHandler(req, res, next) {
  var token = req.headers['x-access-token'];
  console.log('thisi s the token ', token);
  if (token) {
    studentModel.findOne({ token: token }, function (err, data) {
      console.log('tokendata', data);
      if (err) {
        throw err;
      } else {
        if (data) {
          next();
        } else {
          res.status(401).send('invalid token');
        }
      }
    });
  } else {
    console.log('you dont have the token');
    res.status(401).send('Please send token');
  }
}
// function isAuthenticated(req,res,next){
//
//     return next();
//     res.send('user not authorized');
// }
// router.post('/',function(req,res,next){
//   res.send('this is a post respond');
// })
// authenticating the student
router.post('/login', studentController.authenticate);
// registering the student

//router.post('/register', studentController.register);//
//router.get('/verifyemail/:email', studentController.verifyEmail);//

// getting single student detail
// auth.isAuthorised
router.get(
  '/getstudentProfileData/:studentid',
  studentController.getstudentProfileData
);
// inserting the query asked by th customer
router.post('/queryInsert', studentController.queryInsert);

router.get('/deleteStuQuery', studentController.deleteStuQuery);

// getting queries of a partiular student
router.get('/studentQueries/:studentId', studentController.studentQueries);
router.get('/getsinglequery/:slug', studentController.getsinglequery);

router.post('/queryAdmin', studentController.queryAdmin);
router.put('/editprofile/:id', studentController.editprofile);
router.get('/getstudentsqueries', studentController.getStudentsQueries);
router.put('/uploadprofileimage/:id', studentController.uploadProfileImage);
router.get('/removestudentdp/:id', studentController.removeStudentDp);

router.post('/askquerytoadmin', studentController.askQueryToAdmin);
router.get(
  '/getstudentadminqueries/:id',
  studentController.getStudentAdminQueries
);
router.get('/getsingleadminquery/:id', studentController.getSingleAdminQuery);
router.put('/studentreply/:id', studentController.studentAdminQueryReply);
//router.get('/checkstudentemail/:email', studentController.checkStudentEmail);//
router.put('/messagetoteacher/:slug', studentController.messageToTeacher);

// operated by teacher
router.put('/ratestudent/:slug', studentController.rateStudent);

//video chat
router.get('/joinsession/:id', studentController.joinSession);
router.get('/sessionwebhook', studentController.sessionWebhook);

//stripe
router.post('/addcard', studentController.addCard);
router.post('/chargecard', studentController.chargeCard);

module.exports = router;
