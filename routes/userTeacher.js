var express = require('express');
var router = express.Router();
var teacherController = require('../app/controllers/teacher.controller');
var teacherModel = require('../app/models/teacher');

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

// function checkAuth(req, res, next) {
//   var token  = req.headers['x-access-token'];
//   console.log("thisi s the token ",token);
//   if(token){
//     teacherModel.findOne({token:token},function(err,data){
//       console.log("tokendata",data);
//       if(err) {
//         throw err
//       }
//       else{
//         if(data){
//           next()
//         }
//         else{
//           res.json({status:401,message:"invalid token"});
//         }
//       }
//     })
//   }
//   else{
//     console.log("you dont have the token");
//     res.json({status:401,message:"invalid token"});
//   }
// };

router.post('/login',teacherController.authenticate);
router.put('/logout/:id',teacherController.logout);
// registering the student
router.post('/register', teacherController.register);
router.get('/verifyemail/:email',teacherController.verifyEmail);
router.get('/getteacherProfileData/:id',teacherController.getteacherProfileData);
router.put('/editprofile/:id',teacherController.editprofile);
router.put('/uploaddoc/:id',teacherController.uploadDoc);
router.put('/uploadprofileimage/:id',teacherController.uploadProfileImage);
router.get('/removeprofileimage/:id',teacherController.removeProfileImage);
router.post('/askquerytoadmin',teacherController.askQueryToAdmin);
router.get('/getteacheradminqueries/:id',teacherController.getTeacherAdminQueries)
router.get('/getsingleadminquery/:id',teacherController.getSingleAdminQuery);
router.put('/teacherreply/:id',teacherController.teacherAdminQueryReply);
router.put('/applyforjob/:slug',teacherController.applyForJob);
router.get('/getmyjobs/:id',teacherController.getMyJobs);
router.get('/sendteacherprofileforreview/:id',teacherController.sendTeacherProfileForReview);

//chat
router.get('/createsession/:slug', teacherController.createSession); // id of query

// messaging to student
router.put('/messagetostudent/:slug',teacherController.messageToStudent);
// saving the card and the user to stripe and database
router.get('/createuserstripe/:token',teacherController.createUserStripe);
// OPEN
// creating the session token for openTok
// router.get('/createsession', teacherController.createSession);



module.exports = router;
