const aws = require("aws-sdk");
const fs = require("fs");

const { responseMessages } = require("../../config/properties/constants").globalMessages;
const { generateRandStr } = require("../../utils/commonFuns");

const s3Config = appConfig.upload.spaces;

aws.config.update({
	accessKeyId: s3Config.accessKeyId,
	secretAccessKey: s3Config.secretAccessKey,
});
// Create an S3 client setting the Endpoint to DigitalOcean Spaces
const s3 = new aws.S3();

exports.uploadFile = (file, elementName) => {
	// const [fileName, fileExt] = file.name.split('.');

	const mimeType = file.type;
	const [fileType, fileExt] = mimeType.split("/");
	const fileName = `${generateRandStr(65)}.${fileExt}`;

	return new Promise((resolve, reject) => {
		fs.readFile(file.path, (error, fileBuffer) => {
			if (error) {
				return reject(new Error(`${elementName} ${responseMessages.FileUploadError}`));
			}
			const params = {
				Bucket: s3Config.bucket,
				Key: `${s3Config.folder}/${fileName}`,
				Body: fileBuffer,
				ACL: "public-read",
				ContentType: mimeType,
			};

			return s3.putObject(params, (err) => {
				if (err) {
					return reject(new Error(`${elementName} ${responseMessages.FileUploadError}`));
				}

				return resolve({
					file: `${s3Config.filesURL}${fileName}`,
					fileType,
					fileExt,
					fileSize: file.size / 1000, // In KB
				});
			});
		});
	});
};

//      Upload File From URL
exports.uploadFileFromUrl = (file, elementName) => {
	const { fileLocation } = file;

	const key = `${s3Config.folder}/${fileLocation}`;

	return new Promise((resolve, reject) => {
		fs.readFile(fileLocation, (error, fileBuffer) => {
			if (error) {
				return reject(new Error(`${elementName} ${responseMessages.FileUploadError}`));
			}

			const params = {
				Bucket: s3Config.bucket,
				Key: key,
				Body: fileBuffer,
				ACL: "public-read",
				ContentType: file.contentType,
			};

			return s3.putObject(params, (err) => {
				if (err) {
					return reject(new Error(`${elementName} ${responseMessages.FileUploadError}`));
				}

				return resolve({
					file: `${s3Config.filesURL}${file.fileLocation}`,
				});
			});
		});
	});
};
