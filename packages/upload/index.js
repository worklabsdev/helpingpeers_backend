const s3 = require("./s3");
const spaces = require("./spaces");

module.exports = {
	s3,
	spaces,
};
