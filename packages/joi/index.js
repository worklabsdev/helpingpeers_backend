const { validationHandler, errorHandlerView } = require("../../utils/responseHandler");
const { requestLogger } = require("../logging/consoleLogger");

const valOptionsObj = {
	allowUnknown: false,
	abortEarly: true,
};

// ///       Common Validate JOI Function
exports.joiValidate = async (apiRefFull, response, schema, data, opts = {}) => {

	requestLogger(apiRefFull, data, opts);//	Logger For Requests

	let { error } = await schema.validate(data, valOptionsObj);

	if (error) {
		return validationHandler(
			response,
			error.details[0].message,
			{
				apiRefFull
			}
		);
	} else {
		return true;
	}

};

//  ///       Admin Validator
exports.joiValidateOpen = async (apiRefFull, response, schema, data, opts = {}) => {

	requestLogger(apiRefFull, data, opts);//	Logger For Requests

	const valOptions = {
		allowUnknown: true,
		abortEarly: true,
	};

	let { error } = await schema.validate(data, valOptions);

	if (error) {
		return validationHandler(
			response,
			error.details[0].message,
			{
				apiRefFull
			}
		);
	} else {
		return true;
	}

};

// ///       COmmon Validate JOI Function
exports.joiValidateView = async (apiRefFull, response, schema, data, opts = {}) => {

	requestLogger(apiRefFull, data, opts);//	Logger For Requests

	let { error } = await schema.validate(data, valOptionsObj);

	if (error) {
		return errorHandlerView(
			response,
			new Error(error.details[0].message),
			{
				apiRefFull
			}
		);
	} else {
		return true;
	}

};