const razor = require("./razorPay");
const stripe = require("./stripe");

module.exports = {
	razor,
	stripe
};
