const nodemailer = require("nodemailer");

let emailConfig = appConfig.emailConfig[appConfig.emailConfig.name];

const transporter = nodemailer.createTransport(emailConfig);

module.exports = transporter;