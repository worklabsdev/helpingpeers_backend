const studentEndpoints = {
    "verifyEmail": "/stu/verifyemail/{{token}}"
};
exports.studentEndpoints = studentEndpoints;


exports.allEndpoints = {
    student: studentEndpoints
};