let apiPath = {

};

let appConstants = {
	"name": "HelpingPears",
	"fullName": "HelpingPears"
};

let successCodes = {
	"success": "success",
	"failed": "failure",
	"created": "success",
	"authFailed": "failure",
	"validationFailed": "failure",
	"error": "failure",
	"actionFailed": "failure",
	"pageNotFound": "failure"
};

let statusCodes = {
	"success": 200,
	"failed": 500,
	"created": 201,
	"validationFailed": 400,
	"authFailed": 401,
	"error": 500,
	"actionFailed": 400,
	"pageNotFound": 404
};

let globalMessages = {
	"responseMessages": {
		"ErrorMsg": "Something went wrong, please try again",
		"SuccessMsg": "Success",
		"ValidationFailedMsg": "Parameter missing or parameter type is wrong",
		"AuthFailedMsg": "Please login first, to continue",
		"AuthHeaderMissing": "Access token is missing",
		"AccountSuspended": "Your account has been suspended, kindly contact admin",
		"FileUploadError": "not uploaded, please try again",
		"FileUploadSuccess": "uploaded successfully",
		"AccountNotFound": "Sorry these credentials are incorrect",
		"OtpVerificationRequired": "Please verify the otp first, to access this resource",
		"PageNotFound": "Incorrect url",
		"InvalidEmailOrPass": "Sorry either your email or password is incorrect",
		"NothingToUpdate": "There is nothing to update",
		"LinkExpired": "Sorry, this link has expired",
		"LogoutSuccess": "Logged out successfully",
		"FileUploadErrorMsg": " not uploaded, please try again",

		"EmailAlreadyRegMsg": "Sorry, this email is already registered with us. Please login to continue",
		"AccountVerifiedSuccessMsg": "Account verified successfully"
	}
};

let staticValues = {
};

const defaultValues = {
	pageLimit: 10,
	mysqlDtFormat: "YYYY-MM-DD hh:mm:ss"
};

const mimeTypes = {
	'.html': 'text/html',
	'.js': 'text/javascript',
	'.css': 'text/css',
	'.ico': 'image/x-icon',
	'.png': 'image/png',
	'.jpg': 'image/jpeg',
	'.gif': 'image/gif',
	'.svg': 'image/svg+xml',
	'.json': 'application/json',
	'.woff': 'font/woff',
	'.woff2': 'font/woff2'
}

module.exports = {
	apiPath,
	appConstants,
	statusCodes,
	successCodes,
	globalMessages,
	staticValues,
	defaultValues,
	mimeTypes
};
