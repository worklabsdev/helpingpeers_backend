var mongoose = require('mongoose');

//For Mongo Mlab
mongoose.connect(appConfig.db.url, {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true
});

//For localhost
// mongoose.connect('mongodb://resimpli:resimpli%232017@52.39.212.226:27017/resimpli',{useMongoClient:true});

//For localhost
// mongoose.connect('mongodb://localhost:27017/hashlearn');

var db = mongoose.connection;

db.on("error", console.error.bind(console, "MongoDB connection error:"));
db.on('open', function (err) {
  if (!err) {
    console.log("Database Connected");
  }
  else {
    console.log("This is error", err);
  }
});