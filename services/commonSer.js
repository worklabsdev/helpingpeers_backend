///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////

const skip = 0;
const limit = 100;

const defaultSort = { "_id": -1 };
exports.defaultSort = defaultSort;

//  /// Pagination Offset Creator
const pageOffsetCreator = (data) => {

    data.skip = parseInt(data.skip || skip);
    data.limit = parseInt(data.limit || limit);

    return data;
};
exports.pageOffsetCreator = pageOffsetCreator;

///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////