var StudentModel = require('../app/models/student');

///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////

const studentProjectSer = {

};
exports.studentProjectSer = studentProjectSer;

const studentMsgSer = {

};
exports.studentMsgSer = studentMsgSer;

///////////////////////////////
////////////////////     Constants       /////////////////////////
//////////////////////////////

//	///			Get
exports.studentGetSer = (findJSON, populate = null) => {
    return StudentModel.findOne(findJSON, populate).lean();
};

//	///			Create
exports.studentCreateSer = (data) => {
    return StudentModel.create(data);
};

//	///	        Update
exports.studentUpdateSer = (findJSON, newData, options = { new: true }) => {
    return StudentModel.findOneAndUpdate(findJSON, newData, options);
};

//  ///         Count
exports.studentCountSer = (whereJSON) => {
    return StudentModel.count(whereJSON);
};