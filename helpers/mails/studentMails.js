const { i18n } = require("../../packages/i18n");
const { mailInfoLogger, mailErrorLogger } = require("../../packages/logging/consoleLogger");

const emailTransport = require("../../config/email");
const { studentEndpoints } = require("../../config/properties/endpointConstants");


let emailConfig = appConfig.emailConfig;

//  Registeration Email Verify Mail
const studentRegisterMailHelperFunc = (data, odata) => {
    try {

        //  Link
        let link = i18n.__(studentEndpoints.verifyEmail, {
            token: data.token
        });

        link = `${appConfig.baseURL}${link}`;
        //  Link

        var html = `<p>Hello <b>${data.firstname}</b>, Thank you for registering with <b>${appConfig.appName}</b>.<br/>Please verify your email to secure your account <a target='_blank' href='${link}' style=font-size:15px>Verify Email</a> .</p>`;

        let mailOptions = {
            to: data.email,
            from: emailConfig.from,
            subject: `New Registeration, Email Verify`,
            html
        };

        emailTransport.sendMail(mailOptions, (err, response) => {
            if (err) {
                mailErrorLogger(odata.apiRefFull, odata.requestId, err, {
                    data,
                    //mailOptions
                });
            }
            else {
                mailInfoLogger(odata.apiRefFull, odata.requestId, {
                    data,
                    //mailOptions,
                    response
                });
            }
        });

    }
    catch (error) {
        mailErrorLogger(odata.apiRefFull, odata.requestId, error, {
            data,
            //mailOptions
        });
    }
};
exports.studentRegisterMailHelperFunc = studentRegisterMailHelperFunc;