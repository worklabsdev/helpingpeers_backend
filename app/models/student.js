var mongoose = require("mongoose");
var uniqueValidator = require("mongoose-unique-validator");
var bcrypt = require("bcryptjs");
const saltRounds = 10;

var Schema = mongoose.Schema;

var StudentSchema = new Schema(
  {
    firstname: { type: String, trim: true },
    lastname: { type: String, trim: true },
    class: { type: Number, trim: true, default: 1 },
    age: { type: Number, trim: true, default: 4 },

    stripeId: { type: String, trim: true, index: true },

    email: { type: String, unique: false, index: true, email: true },
    gId: { type: String, trim: true, index: true, unique: false },
    fId: { type: String, trim: true, index: true, unique: false },
    lId: { type: String, trim: true, index: true, unique: false },

    password: { type: String, trim: true },
    languages: { type: Array, default: [] },

    token: { type: String, trim: true, index: true, unique: false },

    profileImage: { type: String, default: "" },
    about: { type: String, trim: true },

    rating: {
      totalRating: { type: Number, default: 0 },
      totalPeople: { type: Number, default: 0 }
    },

    emailVerified: { type: Boolean, default: false },
    isEnabled: { type: Boolean, default: true },
    isDeleted: { type: Boolean, default: false },
  },
  { timestamps: true },
  { collection: "student" }
);

StudentSchema.plugin(uniqueValidator);

StudentSchema.pre("save", function (next) {
  var expert = this;
  if (!expert.isModified("password")) {
    return next();
  }
  bcrypt.genSalt(saltRounds, function (err, salt) {
    bcrypt.hash(expert.password, salt, function (err2, hash) {
      expert.password = hash;
      next();
    });
  });

});

module.exports = mongoose.model("student", StudentSchema);