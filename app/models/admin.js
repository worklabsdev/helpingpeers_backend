var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
const saltRounds = 10;

var Schema  = mongoose.Schema;

var AdminSchema = new Schema({
  name      : { type:String,trim:true,default:'admin'},
  email     : { type:String,unique:true},
  token : { type:String,trim:true},
  password  : { type:String,trim:true},
  bank_details:{type:Array,default:[]},
  isEnabled : { type:Boolean, default:true},
  isDeleted : { type:Boolean, default:false}
},{timestamps:true});

AdminSchema.pre('save',function(next){
  var expert = this;
  if(!expert.isModified('password')){
    return next();
  }
  bcrypt.genSalt(saltRounds,function(err,salt){
    bcrypt.hash(expert.password,salt,function(err2,hash){
      expert.password = hash;
      next();
    })
  })
})
module.exports = mongoose.model('admin',AdminSchema);
// module.exports = StudentModel;
