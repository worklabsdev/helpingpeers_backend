var mongoose = require('mongoose');
var slug = require('mongoose-slug-generator');
var Schema = mongoose.Schema;
mongoose.plugin(slug);


var QuerySchema = new Schema({
  title: {type:String,trim:true},
  slug : { type:String,slug:'title'},
  question : {type:String,trim:true},
  sessionId: {type: String, trim:true},
  queryImage : {type:String,trim:true},
  studentId: { type:Schema.Types.ObjectId,ref:'student'},
  requests: {type:Array,default:[]},
  selectedteacher :{ type:Schema.Types.ObjectId,ref:'teacher'},
  ratingandfeedback: {
    comment : {type:String,trim:true},
    rating : {type:Number,trim:true,default:0}
  },
  message:[{
    userType:{type:String,enum:['teacher','expert']},
    replyText:{ type:String,trim:true},
    repliedAt: { type:Date}
}],
  isEnabled : { type:Boolean, default:true },
  isDeleted: { type:Boolean, default:false }
},{timestamps:true});

var QueryModel = mongoose.model('query',QuerySchema);
module.exports = QueryModel;
