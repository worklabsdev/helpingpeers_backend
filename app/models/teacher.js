var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var bcrypt = require('bcryptjs');
const saltRounds = 10;

var Schema  = mongoose.Schema;

var TeacherSchema = new Schema({
  name      : { type:String,trim:true},
  email     : { type:String,unique:true},
  password  : { type:String,trim:true},
  subjects: {type:Array,default:[]},
  title: { type:String,trim:true},
  about:{type:String,trim:true},
  languages: {type:Array,default:[]},
  experience: { type:Number,default:1},
  docs : {
    name: {type:String,trim:true},
    isDegreeApproved: { type:String,enum:['approved','disapproved']}
  },
  profile : {
    status: {type:String,enum:['approved','disapproved']},
    mailsent: { type:Boolean,default:false}
  },
  token: { type: String,trim:true},
  age: {type:Number,trim:true},
  bank_details:{type:Array,default:[]},
  rating : {type:Number, default:0,trim:true},
  isEnabled : { type:Boolean, default:false},
  isDeleted : { type:Boolean, default:false},
  emailverified:{type:Boolean,default:false},
  profileImage : {type:String, default:''},
  gId : {type:String,trim:true},
  fId : {type:String,trim:true},
},{timestamps:true},{'collection':'teacher'});
TeacherSchema.plugin(uniqueValidator);
TeacherSchema.pre('save',function(next){
  var expert = this;
  if(!expert.isModified('password')){
    return next();
  }
  bcrypt.genSalt(saltRounds,function(err,salt){
    bcrypt.hash(expert.password,salt,function(err2,hash){
      expert.password = hash;
      next();
    })
  })
})
module.exports = mongoose.model('teacher',TeacherSchema);
// module.exports = StudentModel;
