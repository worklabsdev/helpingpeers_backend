var mongoose= require('mongoose');
var slug  = require('mongoose-slug-generator');
var Schema = mongoose.Schema;
mongoose.plugin(slug);

var QueryAdminSchema = new Schema({
  title: {type:String,trim:true,required:true},
  slug : {type:String,slug:'title'},
  question:{ type:String,trim:true},
  sender: {
    senderType :{type:String,enum:['teacher','student'],required:true},
    senderId: {type:String,required:true}
  },
  reply : [{
    userType:{type:String,enum:['teacher','student','admin']},
    replyText:{ type:String,trim:true},
    repliedAt: { type:Date}
}],
  isEnabled : { type:Boolean, default:true},
  isDeleted : { type:Boolean, default:false},
},{timestamps:true});

var QueryAdminModel = mongoose.model('queryAdmin',QueryAdminSchema);
module.exports = QueryAdminModel;
