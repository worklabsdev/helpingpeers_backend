var jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
var teacherModel = require('../models/teacher');
var queryModel = require('../models/query');
var queryAdminModel = require('../models/queryAdmin');
var nodemailer  = require('nodemailer');
var async = require('async');
var Handlebars = require('handlebars');
var mongoose = require('mongoose');
var nodemailer = require('nodemailer');
var path    = require("path");
var { projectKey, projectSecret } = require('../../config/constants');

mongoose.Promise = Promise;


var authenticate = function(req,res,next) {
  req.checkBody('email','please send the email').exists().isEmail();
  req.checkBody('password','Password required').exists();
  var errors = req.validationErrors();
  if(errors) {
    return res.status(400).send("Errors",{errors:errors});
  }
  var student ={};
  student['email'] = req.body.email.toLowerCase().trim();
  student['password'] = req.body.password.toLowerCase().trim();
  teacherModel.findOne({email:student['email'],isDeleted:false},function(err,result){
    if(!err) {
      if(result)  {
        bcrypt.compare(student['password'],result.password,(err,match)=>{
          if(err) {
            res.json({message:'Password String does not match'});
          }
          console.log('match',err);
          if(match){
            let userJson = {
              '_id':result._id,
              'name':result.name,
              'email' :result.email
            }
            const token = jwt.sign(userJson,'my_secret_key')
            var response = {
              token :token,
              userdata:userJson
            }
            res.status(200).send(response);
          }
          else {
            res.status(401).send('Password did not match');
          }
        })
      }
      else {
        res.status(401).send('Invalid Credentials');
      }
    }
  })
}
var verifyEmail = function(req,res) {
  req.checkParams('email','Please send student id');
  teacherModel.findOneAndUpdate({email:req.params.email,isDeleted:false},{emailverified:true},function(err,data) {
    if(err) {
      throw err
    }
    else {
      console.log("this is data",data);
      if(data){
         res.sendFile(path.join(__dirname, '../../views/mailVerified.html'));
        // res.send({status:200,'message':'Email verified'});
      }
    }
  })

}

var register = function(req,res) {
  req.checkBody('name','Please send name');
  req.checkBody('email','Please send email');
  req.checkBody('password','Please send password');
  var errors  = req.validationErrors();
  if(errors) {
    return res.status(400).failure("Errors",{errors:errors});
  }
  var teacher = new Object();
  var errorMessage = "";
  teacher.name = req.body.name.toLowerCase();
  teacher.class=req.body.class;
  teacher.age=req.body.age;
  teacher.email = req.body.email;
  teacher.password = req.body.password;
  teacherModel(teacher).save(function(err,user) {
    if(err) {
      console.log("Error While saving user ",err);
      switch (err.name) {
        case 'ValidationError':
        for (field in err.errors) {
          if (errorMessage == "") {
            errorMessage = err.errors[field].message;
          }
          else {
            errorMessage += ", " + err.errors[field].message;
          }
        }//for
        break;
      } //switch
    }
    else {
      nodemailer.createTestAccount((err,account)=>{
        let transporter = nodemailer.createTransport({
          host:'smtp.gmail.com',
          port : 587,
          secure:false,
          auth:{
            user:'sameer.sam9997@gmail.com',
            pass:'sameernida'
          }
        })
        var template =  "<p>Hello, Admin . A new Student has registered <br/>  Name - {{name}}. With email id - {{email}}.</p>";
        var temp = Handlebars.compile(template);
        var context =  { name:user.name ,email:user.email,createdAt: user.createdAt};
        var html = temp(context);
        // var studenttemplate = "<p>Hello,  Thank you for registering with Helping Pears.<br/>Please verify your emiail id to secure your account <a target='_blank' href='http://localhost:3000/t/verifyemail/{{email}}' style=font-size:15px>Verify Email</a> .</p>";
        var studenttemplate = "<p>Hello,  Thank you for registering with Helping Pears.<br/>Please verify your emiail id to secure your account <a target='_blank' href='https://68.183.49.174:3000/t/verifyemail/{{email}}' style=font-size:15px>Verify Email</a> .</p>";

        var stutemp = Handlebars.compile(studenttemplate);
        var stuhtml = stutemp(context);
        let mailOptions = {
          from :'sameer.sam9997@gmail.com',
          to :'sameer.it78@gmail.com',
          subject:'Registeration successfully done',
          text: 'Hello admin',
          html: html
        }
        let expertmailOption = {
          from :'sameer.sam9997@gmail.com',
          to :user.email,
          subject:'Thank you for registering',
          text: 'Hello Expert',
          html: stuhtml
        }
        async.parallel([
          function(callback) {
            transporter.sendMail(expertmailOption,(er,infod)=>{
              if(er) {
                console.log("this is the error  for first mail ",er);
                callback(er);
              }
              // console.log("message sent to the expert ",infod.messageId);
              callback(null);
            })
          },function(callback) {
            transporter.sendMail(mailOptions,(error,info)=>{
              if(error) {
                callback(error);
                console.log(error);
              }
              callback(null);
              // console.log("Message send %s",info.messageId);

              console.log("Preview URL %s",nodemailer.getTestMessageUrl(info));
            })
          }
        ],function(err,results){
          if(err) {
            console.log("got error in this part",err);
          }
          else {
            if(results) {
              console.log("this is the user data",user);
              let userJson = {
                '_id':user._id,
                'name':user.name,
                'email' :user.email
              }
              const token = jwt.sign(userJson,'my_secret_key');
              console.log("this is token",token);
              var response = {
                token :token,
                userdata:userJson
              }
              res.status(200).send(response);
              return res.json({status:200,response});
            }
          }
        })
      })
    }
  })
}

var queryAdmin = function(req,res) {
  req.checkBody('studentId','Please send student Id');
  req.checkBody('query','Please send Query');
  teacherModel.findById(req.body.studentId,function(err,data){
    if(err) {
      throw err;
    }
    else {
      console.log("this is data",data);
    }
  })

  // nodemailer.createTestAccount((err,account)=>{
  //   let transporter = nodemailer.createTransport({
  //     host:'smtp.gmail.com',
  //     port : 587,
  //     secure:false,
  //     auth:{
  //       user:'sameer.sam9997@gmail.com',
  //       pass:'sameernida'
  //     }
  //   })
  //   var template =  "<p>Hello, Admin . A new Student has this query for you QUERY :-  has added a article <br/>  Name - {{name}}.</br> Email Id - {{email}}. </br> Created At "+
  //   "{{createdAt}}:</p> </br> <p>please Click <a href='http://159.65.145.191:3000/adminapproval/{{id}}'><u> Here </u> </a> to verify this very article</p>";
  //   var temp = Handlebars.compile(template);
  //   var context =  { name:expertdata.name ,email:expertdata.email,createdAt: expertdata.createdAt,id:articledata._id};
  //   var html = temp(context);
  //   let mailOptions = {
  //     from :'sameer.sam9997@gmail.com',
  //     to :'sameer.it78@gmail.com',
  //     subject:'Article For approval',
  //     text: 'Article Approval ',
  //     html: html
  //   }
  //   transporter.sendMail(mailOptions,(error,info)=>{
  //     if(error) {
  //       return console.log(error);
  //     }
  //     console.log("Message send %s",info.messageId);
  //     return res.status(200).json({message:'User Saved successfully ',mailoption:info});
  //     console.log("Preview URL %s",nodemailer.getTestMessageUrl(info));
  //   })
  // })


}

var getteacherProfileData = function(req,res) {
  req.checkParams('id','Please send teacher id');
  var id = req.params.id;
  teacherModel.find({_id:id,isDeleted:false},function(err,data){
    if(err) throw err
    else {
      console.log('thisis the data',data);
      res.json({status:200,message:'getting teacher data',data});
    }
  })
}

var editprofile= function(req,res) {
  req.checkParams('id','student id not received');
  req.checkBody('firstname','first name required');
  req.checkBody('class','class required');
  req.checkBody('age','Age required');
  console.log("these are languages",req.body.language);
  var profiledata = {
    firstname:req.body.firstname,
    class:req.body.class,
    age:req.body.age,
    languages:req.body.language,
    about:req.body.about ? req.body.about :''
  }

  teacherModel.findOneAndUpdate({_id:req.params.id},profiledata,(err,data)=>{
    if(err) {
      throw err
    }
    else {
      console.log("this is the update data",data);
      res.json({status:200,message:'data update',data});
    }
  })
}


var editprofile= function(req,res) {
  req.checkParams('id','Teacher id not received');
  console.log("these are languages",req.body.language);
  var profiledata = {
    name:req.body.name,
    experience: req.body.experience,
    title:req.body.title,
    age:req.body.age,
    languages:req.body.language,
    subjects : req.body.subjects ? req.body.subjects : '',
    about:req.body.about ? req.body.about : ''
  }

  teacherModel.findOneAndUpdate({_id:req.params.id},profiledata,{upsert:true},(err,data)=>{
    if(err) {
      throw err
    }
    else {
      console.log("this is the update data",data);
      res.json({status:200,message:'data update',data});
    }
  })
}

var uploadDoc = function(req,res) {
  req.checkParams('id','teacher id not received');
  req.checkBody('docs','docs not received');
  var id = req.params.id;
  var profile_base64Data = req.body.docs.replace(/^data:image\/png;base64,/, "");
  teacherModel.findById(id,(err,data)=>{
    if(err) {
      res.send(err);
    }
    else {
      console.log("first phase data",data);
      if(data.docs.name){
        console.log("another process started",data.docs.name);
        require("fs").unlinkSync("public/teacher/docs/" + data.docs.name,(derr,dat)=>{
          if(derr) {
            console.log("This is the error ",derr);
          }
          console.log("image removed",dat);
        })
        var complete_image_url = id + Math.floor(Math.random() * 600)+1 + '.png';
        require("fs").writeFile("public/teacher/docs/" + complete_image_url,profile_base64Data,'base64',function(gerr){
          if(gerr) {
            console.log("error while inserting the profile image for the first time ");
          }
          // var dtdata={
          //   docs.name: complete_image_url
          // }
          teacherModel.findByIdAndUpdate(id,{"docs.name":complete_image_url,"docs.isDegreeApproved":''},{new: true},(err,data)=>{
            if(err) {
              res.send(err);
            }
            else {
              if(data) {
                res.json({status:200,message:'teacher doc updated successfully',data});
              }
            }
          })
        })
      }
      else {
        var complete_image_url = id + Math.floor(Math.random() * 500)+1 + '.png';
        // var ddata= {
        //   docs :{
        //   name: complete_image_url
        //   }
        // }
        require("fs").writeFile("public/teacher/docs/" + complete_image_url,profile_base64Data,'base64',function(gerr){
          if(gerr) {
            console.log("error while inserting the profile image for the first time ");
          }
          teacherModel.findByIdAndUpdate(id,{"docs.name":complete_image_url},{new: true},(err,data)=>{
            if(err) {
              res.send(err);
            }
            else {
              if(data) {
                res.json({status:200,message:'Teacher docs updated successfully',data});
              }
            }
          })

        })
      }
    }
  })
}

var uploadProfileImage = function(req,res) {
  req.checkParams('id','teacher id not received');
  req.checkBody('profileImage','docs not received');
  var id = req.params.id;
  console.log("this is the profile image",req.body.profileImage);
  var profile_base64Data = req.body.profileImage.replace(/^data:image\/png;base64,/, "");
  teacherModel.findById(id,(err,data)=>{
    if(err) {
      res.send(err);
    }
    else {
      console.log("first phase data",data);
      if(data.profileImage){
        console.log("another process started");
        require("fs").unlinkSync("public/teacher/dp/" + data.profileImage,(derr,dat)=>{
          if(derr) {
            console.log("This is the error ",derr);
          }
          console.log("image removed",dat);
        })
        var complete_image_url = id + Math.floor(Math.random() * 600)+1 + '.png';
        require("fs").writeFile("public/teacher/dp/" + complete_image_url,profile_base64Data,'base64',function(gerr){
          if(gerr) {
            console.log("error while inserting the profile image for the first time ");
          }
          var dtdata={
            profileImage: complete_image_url
          }
          teacherModel.findByIdAndUpdate(id,dtdata,{new: true},(err,data)=>{
            if(err) {
              res.send(err);
            }
            else {
              if(data) {
                res.json({status:200,message:'teacher doc updated successfully',data});
              }
            }
          })
        })
      }
      else {
        var complete_image_url = id + Math.floor(Math.random() * 500)+1 + '.png';
        var ddata= {
          profileImage :complete_image_url
        }
        require("fs").writeFile("public/teacher/dp/" + complete_image_url,profile_base64Data,'base64',function(gerr){
          if(gerr) {
            console.log("error while inserting the profile image for the first time ");
          }
          teacherModel.findByIdAndUpdate(id,ddata,{new: true},(err,data)=>{
            if(err) {
              res.send(err);
            }
            else {
              if(data) {
                res.json({status:200,message:'Teacher docs updated successfully',data});
              }
            }
          })

        })
      }
    }
  })
}

var removeProfileImage =  function(req,res) {
  req.checkParams('id','teacher id not received');
  var id = req.params.id;
  // var profile_base64Data = req.body.profileImage.replace(/^data:image\/png;base64,/, "");
  teacherModel.findById(id,(err,data)=>{
    if(err) {
      res.send(err);
    }
    else {
      console.log("first phase data",data);
      if(data.profileImage){
        console.log("another process started");
        require("fs").unlink("public/teacher/dp/" + data.profileImage,(derr,dat)=>{
          if(derr) {
            console.log("This is the error ",derr);
          }
          console.log("image removed",dat);
          var postdata  = {
            profileImage : ''
          }
          teacherModel.findOneAndUpdate({_id:id},{postData},(err,data)=>{
            if(err) throw err;
            else  {
              console.log("this is the data",data);
              res.json({status:200,message:'Profile image removed',data});
            }
          })
        })
      }
    }
  })
}
var logout=function(req,res){
  req.checkParams('id','teacher id required');
  var id = req.params.id;
  teacherModel.findOneAndUpdate({_id:id},{token:''},function(err,data){
    if(err) throw err;
    else {
      console.log("this is the data",data);
      res.json({status:200,message:'successfully logged out'});
    }
  })
}
var askQueryToAdmin = function(req,res) {
  req.checkBody('title','Query Title Required');
  req.checkBody('question','Question required');
  req.checkBody('sender.senderId','sender id is required');
  console.log("thisi s the request",req.body.sender.senderId);
  var postJson ={
    title:req.body.title,
    question:req.body.question,
    sender : {
      senderId:req.body.sender.senderId,
      senderType:'teacher'
    }
  }
  queryAdminModel(postJson).save(function(err,data) {
    if(err) throw err;
    else {
      console.log('data',data);
      res.json({status:200,message:'this is the data',data});
    }
  })
}

var getTeacherAdminQueries = function (req,res) {
  req.checkParams('id','Please send student id ');
  var  id = req.params.id;
  queryAdminModel.find({"sender.senderId":id}).sort('-createdAt').exec(function(err,data){
    if(err)throw err;
    else{
      if(data){
        console.log("you got the data",data);
        res.json({status:200,message:'you got all the queries',data});
      }
    }
  })
}

var getSingleAdminQuery = function(req,res) {
  req.checkParams('id','Please send id');
  var id  = req.params.id;
  queryAdminModel.findOne({_id:id,isDeleted:false},function(err,data){
    if(err) throw err;
    console.log("this is data",data);
    res.json({status:200,message:'getting all admin queries ',data});
  })
}
var teacherAdminQueryReply =function(req,res) {
  req.checkParams('id','Please send the id');
  req.checkBody('userType','Please send usertype');
  req.checkBody('replyText','Please send reply text');
  req.checkBody('repliedAt','Please send reply time');
  var postData = {
    userType : req.body.userType,
    replyText: req.body.replyText,
    repliedAt : req.body.repliedAt
  };
  var id = req.params.id;
  queryAdminModel.findOneAndUpdate({_id:id},{ $push: {reply : postData}},function(err,data)
  {
    if(err) throw err;
    console.log("data updated",data);
    res.json({status:200,message:'data updated',data});
  })
}

var applyForJob = function(req,res) {
  console.log("this is req.body",req.body);
  req.checkParams('slug','Please send slug to indentify');
  req.checkBody('selectedteacher','Please send selected teacher');
  queryModel.findOneAndUpdate({slug:req.params.slug,isDeleted:false},{selectedteacher:req.body.selectedteacher},function(err,data){
    if(err) throw err;
    console.log("data updated",data);
    res.json({status:200,message:'Data updated successfully',data});
  })
}
var getMyJobs = function(req,res) {
  req.checkParams('id','PLease send id ');
  var id = mongoose.Types.ObjectId(req.params.id);
  console.log("this is the getMYJOBS ID",id);
  queryModel.find({isDeleted:false,selectedteacher:{$exists:true,$eq:id}},{q:0})
  .populate('studentId',['firstname', 'profileImage'])
  .exec(function(err,data){
    if(err) throw err;
    console.log("this is data population",data);
    res.json({status:200,message:'Getting my jobs',data});
  })
}

var messageToStudent = function(req,res) {
  req.checkParams('slug','Please send query slug ');
  req.checkBody('userType','Please send usertype');
  req.checkBody('replyText','Please send reply text');
  req.checkBody('repliedAt','Please send reply time');

  var postData = {
    userType : req.body.userType,
    replyText: req.body.replyText,
    repliedAt : req.body.repliedAt
  };
  var slug= req.params.slug;
  queryModel.findOneAndUpdate({slug:slug},{ $push: {message : postData}},function(err,data)
  {
    if(err) throw err;
    console.log("data updated",data);
    res.json({status:200,message:'data updated',data});
  })
}

var sendTeacherProfileForReview = function(req,res) {
  req.checkParams('id','send teacher for approval');
  let id = req.params.id;
  teacherModel.findById(id,(err,data)=>{
    if(err) throw err;
    console.log("this is teacher data",data);
    nodemailer.createTestAccount((err,account)=>{
      let transporter = nodemailer.createTransport({
        host:'smtp.gmail.com',
        port : 587,
        secure:false,
        auth:{
          user:'sameer.sam9997@gmail.com',
          pass:'sameernida'
        }
      })
      var template =  "<p>Hello, Admin . A new Student has sent profile for review <br/>  Name - {{name}}. With email id - {{email}}. <br/> Please visit your dashboard for more details ...</p>";
      var temp = Handlebars.compile(template);
      var context =  { name:data.name ,email:data.email,createdAt: data.createdAt};
      var html = temp(context);
      let mailOptions = {
        from :'sameer.sam9997@gmail.com',
        to :'sameer.it78@gmail.com',
        subject:'New profile for review',
        // text: 'Hello admin',
        html: html
      }
      transporter.sendMail(mailOptions,(er,infod)=>{
        if(er) {
          console.log("this is the error  for first mail ",er);
          callback(er);
        }
        if(infod.messageId) {
          console.log("message sent to the expert ",infod.messageId);
          teacherModel.findOneAndUpdate({_id:id},{"profile.mailsent":true,"profile.status":''},function(lerr,ldata){
            if(lerr) throw lerr;
            console.log("this is data",ldata);
            res.json({status:200,message:'mail sent and updated status'});
          })
        }
      })
    }) // end of createTestConnect
  })
}

var createUserStripe = function(req,res) {
 console.log("you got the stripe token data",req.params.token);
 res.send(res.params.token);
}
var createSession = function(req, res) {
  // console.log(req.params.slug);
  // req.checkParams('slug','send query ID to create a video session');
  // opentok.createSession({ mediaMode: 'routed' }, async function(err, session) {
  //   if (err) throw err;
  //   const options = {
  //     role : 'moderator',
  //     data : 'teacher'
  //   }
  //   const sessionId = await session.sessionId;
  //   const token = await session.generateToken(sessionId, options);
  //   // queryModel.findByIdAndUpdate(req.params.id, {sessionId: sessionId}, { new: true }, function(err, data) {
  //   queryModel.findOneAndUpdate({slug : req.params.slug}, {sessionId: sessionId}, { new: true }, function(err, data) {
  //     if (err) throw err;
  //     else {
  //       const newData = {...data._doc, token_op: token}
  //       // data.token_op = token;
  //       res.send(newData);
  //     }
  //   })
  // });
}



module.exports =  {
  'authenticate' : authenticate,
  'register': register,
  'verifyEmail':verifyEmail,
  // 'getteacherProfileData' : getteacherProfileData,
  'queryAdmin' : queryAdmin,
  'editprofile' : editprofile,
  'getteacherProfileData' :getteacherProfileData,
  'editprofile':editprofile,
  'uploadDoc':uploadDoc,
  'uploadProfileImage' :uploadProfileImage,
  'removeProfileImage' : removeProfileImage,
  'logout' : logout,
  'askQueryToAdmin':askQueryToAdmin,
  'getTeacherAdminQueries' :getTeacherAdminQueries,
  'getSingleAdminQuery' :getSingleAdminQuery,
  'teacherAdminQueryReply' :teacherAdminQueryReply,
  'applyForJob' :applyForJob,
  'getMyJobs':getMyJobs,
  'messageToStudent':messageToStudent,
  'sendTeacherProfileForReview' :sendTeacherProfileForReview,
  'createUserStripe' :createUserStripe,
  'createSession': createSession

  // 'getStudentsQueries' :getStudentsQueries
}
