// Importing All the models
var adminModel = require("../models/admin");
var studentModel = require("../models/student");
var teacherModel = require("../models/teacher");
var queryModel = require("../models/query");
var queryAdminModel = require("../models/queryAdmin");
//-----------------------------------------
// ------------------------- Third Party libraries
var jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
var nodemailer = require("nodemailer");
var async = require("async");
var Handlebars = require("handlebars");
var mongoose = require("mongoose");
var nodemailer = require("nodemailer");
mongoose.Promise = Promise;
// -----------------------------------------

var authenticate = function(req, res, next) {
  req
    .checkBody("email", "please send the email")
    .exists()
    .isEmail();
  req.checkBody("password", "Password required").exists();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).send("Errors", { errors: errors });
  }
  var admin = {};

  admin["email"] = req.body.email.toLowerCase().trim();
  admin["password"] = req.body.password.toLowerCase().trim();
  adminModel.findOne({ email: admin["email"], isDeleted: false }, function(
    err,
    result
  ) {
    console.log("err", err);
    if (!err) {
      console.log("result", result);
      if (result) {
        console.log("thisi s iresult", result);
        bcrypt.compare(admin["password"], result.password, (err, match) => {
          if (err) {
            res.json({ message: "Password String does not match" });
          }
          console.log("match", err);
          if (match) {
            let userJson = {
              _id: result._id,
              name: result.name,
              email: result.email
            };
            const token = jwt.sign(userJson, "my_secret_key");
            var response = {
              token: token,
              userdata: userJson
            };
            res.status(200).send(response);
          } else {
            res.status(401).send("Password did not match");
          }
        });
      } else {
        res.status(401).send("Invalid Credentials");
      }
    }
  });
};

var register = function(req, res) {
  req.checkBody("name", "Please send name");
  req.checkBody("email", "Please send email");
  req.checkBody("password", "Please send password");
  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure("Errors", { errors: errors });
  }
  var admin = new Object();
  var errorMessage = "";
  admin.firstname = req.body.name.toLowerCase();
  admin.email = req.body.email;
  admin.password = req.body.password;
  adminModel(admin).save(function(err, user) {
    if (err) {
      console.log("Error While saving user ", err);
      switch (err.name) {
        case "ValidationError":
          for (field in err.errors) {
            if (errorMessage == "") {
              errorMessage = err.errors[field].message;
            } else {
              errorMessage += ", " + err.errors[field].message;
            }
          } //for
          break;
      } //switch
    } else {
      console.log("admin created", user);
      res.send(user);
      // nodemailer.createTestAccount((err,account)=>{
      //   let transporter = nodemailer.createTransport({
      //     host:'smtp.gmail.com',
      //     port : 587,
      //     secure:false,
      //     auth:{
      //       user:'sameer.sam9997@gmail.com',
      //       pass:'sameernida'
      //     }
      //   })
      //   var template =  "<p>Hello, Admin . A new Student has registered <br/>  Name - {{name}}. With email id - {{email}}.</p>";
      //   var temp = Handlebars.compile(template);
      //   var context =  { name:user.name ,email:user.email,createdAt: user.createdAt};
      //   var html = temp(context);
      //   var studenttemplate = "<p>Hello,  Thank you for registering with Helping Pears.<br/>Please verify your emiail id to secure your account <a target='_blank' href='http://localhost:3000/stu/verifyemail/{{email}}' style=font-size:15px>Verify Email</a> .</p>";
      //   var stutemp = Handlebars.compile(studenttemplate);
      //   var stuhtml = stutemp(context);
      //   let mailOptions = {
      //     from :'sameer.sam9997@gmail.com',
      //     to :'sameer.it78@gmail.com',
      //     subject:'Registeration successfully done',
      //     text: 'Hello admin',
      //     html: html
      //   }
      //   let expertmailOption = {
      //     from :'sameer.sam9997@gmail.com',
      //     to :user.email,
      //     subject:'Thank you for registering',
      //     text: 'Hello Expert',
      //     html: stuhtml
      //   }
      //   async.parallel([
      //     function(callback) {
      //       transporter.sendMail(expertmailOption,(er,infod)=>{
      //         if(er) {
      //           console.log("this is the error  for first mail ",er);
      //           callback(er);
      //         }
      //         console.log("message sent to the expert ",infod.messageId);
      //         callback(null);
      //       })
      //     },function(callback) {
      //       transporter.sendMail(mailOptions,(error,info)=>{
      //         if(error) {
      //           callback(error);
      //           console.log(error);
      //         }
      //         callback(null);
      //         console.log("Message send %s",info.messageId);
      //
      //         console.log("Preview URL %s",nodemailer.getTestMessageUrl(info));
      //       })
      //     }
      //   ],function(err,results){
      //     if(err) {
      //       console.log("got error in this part",err);
      //     }
      //     else {
      //       if(results) {
      //         return res.json({status:200,message:'Mail sent successfully'});
      //       }
      //     }
      //   })
      // })
    }
  });
};

var getAllStudents = function(req, res) {
  studentModel.find({ isDeleted: false }, function(err, data) {
    if (err) {
      throw err;
    } else {
      if (data) {
        console.log("this is the data student", data);
        res.json({ status: 200, message: "getting students", data });
      }
    }
  });
};

var getAllTeachers = function(req, res) {
  teacherModel.find(
    {
      isDeleted: false,
      "profile.mailsent": true,
      "profile.status": "approved"
    },
    function(err, data) {
      console.log("error", err);
      if (err) {
        throw err;
      } else {
        if (data) {
          console.log("this is the data", data);
          res.json({ status: 200, message: "getting students", data });
        }
      }
    }
  );
};
var getSingleTeacher = function(req, res) {
  req.checkParams("name", "teacher name required");
  req.checkParams("email", "teacher email required");
  let name = req.params.name;
  let email = req.params.email;
  teacherModel.findOne({ name: name, email: email, isDeleted: false }, function(
    err,
    data
  ) {
    if (err) {
      throw err;
    } else {
      if (data) {
        console.log("this is the data", data);
        res.json({ status: 200, data });
      }
    }
  });
};
var getSingleStudent = function(req, res) {
  req.checkParams("id", "student id required");
  let id = req.params.id;
  studentModel.findOne({ _id: id, isDeleted: false }, function(err, data) {
    if (err) {
      throw err;
    } else {
      if (data) {
        console.log("this is the data", data);
        res.json({ status: 200, data });
      }
    }
  });
};

var teacherDegreeApprove = function(req, res) {
  req.checkParams("id", "send teacher id");
  req.checkBody("status", "send degree status");
  var id = req.params.id;
  var status = req.body.status;
  if (status == "approved") {
    teacherModel.findOneAndUpdate(
      { _id: id },
      { "docs.isDegreeApproved": status },
      function(err, data) {
        if (err) throw err;
        console.log("data updated", data);
        res.json({ status: 200, message: "data updated", data });
      }
    );
  } else if (status == "disapproved") {
    teacherModel.findOneAndUpdate(
      { _id: id },
      { "docs.isDegreeApproved": status, "profile.status": status },
      function(err, data) {
        if (err) throw err;
        console.log("data updated", data);
        res.json({ status: 200, message: "data updated", data });
      }
    );
  }
};

var getAllAdminQueries = function(req, res) {
  queryAdminModel
    .find({ isDeleted: false })
    .sort("-createdAt")
    .exec(function(err, data) {
      if (err) throw err;
      console.log("this is the data", data);
      res.json({ status: 200, message: "Queries to Admin ", data });
    });
};

var getSingleAdminQuery = function(req, res) {
  req.checkParams("id", "Query id required");
  var id = req.params.id;
  queryAdminModel.find({ _id: id, isDeleted: false }, function(err, data) {
    if (err) throw err;
    console.log("thsi si the data", data);
    res.json({ status: 200, message: "SIngle query data", data });
  });
};

var adminReply = function(req, res) {
  req.checkParams("id", "Please send the id");
  req.checkBody("userType", "Please send usertype");
  req.checkBody("replyText", "Please send reply text");
  req.checkBody("repliedAt", "Please send reply time");
  var postData = {
    userType: req.body.userType,
    replyText: req.body.replyText,
    repliedAt: req.body.repliedAt
  };
  var id = req.params.id;
  queryAdminModel.findOneAndUpdate(
    { _id: id },
    { $push: { reply: postData } },
    function(err, data) {
      if (err) throw err;
      console.log("data updated", data);
      res.json({ status: 200, message: "data updated", data });
    }
  );
};
var enableDisableStudent = function(req, res) {
  req.checkParams("id", "Please send the id of student");
  req.checkBody("status", "please send the stauts");
  var id = req.params.id;
  var status = req.body.isEnabled;
  studentModel.findOneAndUpdate({ _id: id }, { isEnabled: status }, function(
    err,
    data
  ) {
    if (err) throw err;
    console.log("this is the data", data);
    res.json({ status: 200, message: "Status of student updated", data });
  });
};
var enableDisableTeacher = function(req, res) {
  req.checkParams("id", "Please send the id of student");
  req.checkBody("status", "please send the stauts");
  var postData = {};
  var id = req.params.id;
  var status = req.body.isEnabled;
  console.log("status", status);
  if (status == true) {
    teacherModel.findOneAndUpdate(
      { _id: id },
      { isEnabled: true, "profile.status": "approved" },
      function(err, data) {
        if (err) throw err;
        else if (data.length) {
          console.log("this is the data", data);
          res.json({ status: 200, message: "Status of teacher updated", data });
        } else {
          res.json({ status: 201, message: "No Records" });
        }
      }
    );
  } else if (status == false) {
    teacherModel.findOneAndUpdate({ _id: id }, { isEnabled: false }, function(
      err,
      data
    ) {
      if (err) throw err;
      else if (data.length) {
        console.log("this is the data", data);
        res.json({ status: 200, message: "Status of teacher updated", data });
      } else {
        res.json({ status: 201, message: "No Records" });
      }
    });
  }
};
// $or:{"profile.status":{$exists:false}},{"profile.status":{$exists:true,$ne:true}}
var getPendingTeachers = (req, res) => {
  teacherModel.find(
    {
      isDeleted: false,
      "profile.mailsent": true,
      "profile.status": { $exists: true },
      "profile.status": ""
    },
    (err, data) => {
      if (err) throw err;
      console.log("this is data", data);
      res.json({ status: 200, message: "Status of teacher updated", data });
    }
  );
};

var getPendingTeachersCount = function(req, res) {
  teacherModel.count(
    {
      isDeleted: false,
      "profile.mailsent": true,
      "profile.status": { $exists: true },
      "profile.status": ""
    },
    (err, data) => {
      if (err) throw err;
      console.log("this is the pending count ", data);
      res.json({
        status: 200,
        message: "getting count of the pending teachers",
        data
      });
    }
  );
};
var getTeacherCount = function(req, res) {
  teacherModel.find({ isDeleted: false }, { createdAt: 1 }, function(
    err,
    data
  ) {
    if (err) throw err;
    else {
      console.log("this is the teacher countr ", data);
      res.json({ status: 200, message: "sending your ", data });
    }
  });
};
var getStudentCount = function(req, res) {
  studentModel.find({ isDeleted: false }, { createdAt: 1 }, function(
    err,
    data
  ) {
    if (err) throw err;
    else {
      console.log("this is the teacher countr ", data);
      res.json({ status: 200, message: "sending your ", data });
    }
  });
};
module.exports = {
  authenticate: authenticate,
  register: register,
  getAllStudents: getAllStudents,
  getAllTeachers: getAllTeachers,
  getSingleTeacher: getSingleTeacher,
  getSingleStudent: getSingleStudent,
  teacherDegreeApprove: teacherDegreeApprove,
  getAllAdminQueries: getAllAdminQueries,
  getSingleAdminQuery: getSingleAdminQuery,
  adminReply: adminReply,
  enableDisableStudent: enableDisableStudent,
  enableDisableTeacher: enableDisableTeacher,
  getPendingTeachers: getPendingTeachers,
  getPendingTeachersCount: getPendingTeachersCount,
  getTeacherCount: getTeacherCount,
  getStudentCount: getStudentCount
};
