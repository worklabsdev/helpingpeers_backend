var jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
var studentModel = require('../models/student');
var queryModel = require('../models/query');
var queryAdminModel = require('../models/queryAdmin');
var nodemailer = require('nodemailer');
var async = require('async');
var Handlebars = require('handlebars');
var mongoose = require('mongoose');
var nodemailer = require('nodemailer');
var { projectKey, projectSecret } = require('../../config/constants');
// var OpenTok = require('opentok'),
//   opentok = new OpenTok(projectKey, projectSecret);
mongoose.Promise = Promise;

var smtpTransport = require("../../config/email");

var stripe = require('stripe')('sk_test_yyurbjQNgeQyMK6a82cHTJiV');

var authenticate = function (req, res, next) {
  req
    .checkBody('email', 'please send the email')
    .exists()
    .isEmail();
  req.checkBody('password', 'Password required').exists();
  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).send('Errors', { errors: errors });
  }
  var student = {};
  student['email'] = req.body.email.toLowerCase().trim();
  student['password'] = req.body.password.toLowerCase().trim();
  studentModel.findOne({ email: student['email'], isDeleted: false }, function (
    err,
    result
  ) {
    if (!err) {
      if (result) {
        bcrypt.compare(student['password'], result.password, (err, match) => {
          if (err) {
            res.json({ message: 'Password String does not match' });
          }
          console.log('match', err);
          if (match) {
            let userJson = {
              _id: result._id,
              firstname: result.firstname,
              email: result.email
            };
            const token = jwt.sign(userJson, 'my_secret_key');
            var response = {
              token: token,
              userdata: userJson
            };
            studentModel.findOneAndUpdate(
              { email: student['email'] },
              { token: token },
              (err, lresult) => {
                if (err) throw err;
                console.log('update the token in database', lresult);
                res.status(200).send(response);
              }
            );
          } else {
            res.status(401).send('Password did not match');
          }
        });
      } else {
        res.status(401).send('Invalid Credentials');
      }
    }
  });
};
var verifyEmail = function (req, res) {
  req.checkParams('email', 'Please send student id');
  studentModel.findOneAndUpdate(
    { email: req.params.email, isDeleted: false },
    { emailVerified: true },
    function (err, data) {
      if (err) {
        throw err;
      } else {
        console.log('this is data', data);
        if (data) {
          res.send({ status: 200, message: 'Email verified' });
        }
      }
    }
  );
};

var register = function (req, res) {
  req.checkBody('firstname', 'Please send name');
  req.checkBody('email', 'Please send email');
  req.checkBody('class', 'Please send class');
  req.checkBody('age', 'Please send age');
  req.checkBody('password', 'Please send password');
  var errors = req.validationErrors();
  if (errors) {
    return res.status(400).failure('Errors', { errors: errors });
  }
  var student = new Object();
  var errorMessage = '';
  student.firstname = req.body.firstname.toLowerCase();
  student.class = req.body.class;
  student.age = req.body.age;
  student.email = req.body.email;
  student.password = req.body.password;
  studentModel(student).save(function (err, user) {
    if (err) {
      console.log('Error While saving user ', err);
      switch (err.name) {
        case 'ValidationError':
          for (field in err.errors) {
            if (errorMessage == '') {
              errorMessage = err.errors[field].message;
            } else {
              errorMessage += ', ' + err.errors[field].message;
            }
          } //for
          break;
      } //switch
    } else {
      nodemailer.createTestAccount((err, account) => {

        var template =
          '<p>Hello, Admin . A new Student has registered <br/>  Name - {{name}}. With email id - {{email}}.</p>';
        var temp = Handlebars.compile(template);
        var context = {
          name: user.name,
          email: user.email,
          createdAt: user.createdAt
        };
        var html = temp(context);
        var studenttemplate =
          "<p>Hello,  Thank you for registering with Helping Pears.<br/>Please verify your emiail id to secure your account <a target='_blank' href='https://68.183.49.174:3000/stu/verifyemail/{{email}}' style=font-size:15px>Verify Email</a> .</p>";
        var stutemp = Handlebars.compile(studenttemplate);
        var stuhtml = stutemp(context);
        let mailOptions = {
          from: 'sameer.sam9997@gmail.com',
          to: 'sameer.it78@gmail.com',
          subject: 'Registeration successfully done',
          text: 'Hello admin',
          html: html
        };
        let expertmailOption = {
          from: 'sameer.sam9997@gmail.com',
          to: user.email,
          subject: 'Thank you for registering',
          text: 'Hello Expert',
          html: stuhtml
        };
        async.parallel(
          [
            function (callback) {
              smtpTransport.sendMail(expertmailOption, (er, infod) => {
                if (er) {
                  console.log('this is the error  for first mail ', er);
                  callback(er);
                }
                // console.log("message sent to the expert ",infod.messageId);
                callback(null);
              });
            },
            function (callback) {
              smtpTransport.sendMail(mailOptions, (error, info) => {
                if (error) {
                  callback(error);
                  console.log(error);
                }
                callback(null);
                // console.log("Message send %s",info.messageId);

                console.log(
                  'Preview URL %s',
                  nodemailer.getTestMessageUrl(info)
                );
              });
            }
          ],
          function (err, results) {
            if (err) {
              console.log('got error in this part', err);
            } else {
              if (results) {
                let userJson = {
                  _id: user._id,
                  name: user.firstname,
                  email: user.email
                };
                const token = jwt.sign(userJson, 'my_secret_key');
                console.log('this is token', token);
                var response = {
                  token: token,
                  userdata: userJson
                };
                studentModel.findOneAndUpdate(
                  { email: student['email'] },
                  { token: token },
                  (err, lresult) => {
                    if (err) throw err;
                    console.log('update the token in database', lresult);
                    res.status(200).send(response);
                  }
                );
                // return res.json({status:200,message:'Mail sent successfully'});
              }
            }
          }
        );
      });
    }
  });
};

// var getstudentProfileData = function(req,res) {
//   req.checkParams('studentid','Please send the expert id');
//   var studentId = req.params.studentid;
//   studentModel
//   .findById({studentId}).then(data =>respondCreated.call(res,data))
//   .catch(err => respondFailedValidation.call(res,err.toString()))
// }

var getstudentProfileData = function (req, res) {
  req.checkParams('studentid', 'Please send the expert id');
  var studentId = req.params.studentid;

  studentModel.find({ _id: studentId }, function (err, data) {
    if (err) {
      console.log('thisi sis the error', err);
    } else {
      res.json({ status: 200, message: 'this is teh data', data });
    }
  });
};

var studentQueries = function (req, res) {
  req.checkParams('studentId', 'params present');

  // var stid = mongoose.Types.ObjectId(req.params.studentId)
  console.log(
    'this is stid',
    mongoose.Types.ObjectId.isValid(req.params.studentId)
  );
  queryModel
    .find({ studentId: req.params.studentId, isDeleted: false })
    .populate('studentId')
    .exec((err, data) => {
      if (err) {
        throw err;
      } else {
        if (data.length) {
          res.json({
            status: 200,
            message: 'getting student queries',
            data: data
          });
        } else {
          res.json({ message: 'No Queries Present', data: data });
        }
      }
    });
};

var getsinglequery = function (req, res) {
  req.checkParams('slug', 'slug not received');
  var slug = req.params.slug;
  queryModel
    .findOne({ slug: slug, isDeleted: false })
    .populate('studentId', ['firstname', 'profileImage', '_id'])
    .populate('selectedteacher', ['name', 'profileImage', '_id'])
    .exec((err, data) => {
      if (err) {
        throw err;
      } else {
        if (data) {
          console.log('this is data', data);
          res.json({
            status: 200,
            message: 'getting student queries',
            data: data
          });
        } else {
          res.json({ message: 'No Queries Present', data: data });
        }
      }
    });
};

var deleteStuQuery = function (req, res) {
  req.checkParams('queryid', 'query id not received');
  var queryId = req.params.queryId;
  queryModel.findOneAndUpdate({ _id: queryId }, { isDeleted: true }, function (
    lsterr,
    lstdata
  ) {
    if (lsterr) {
      res.send(lsterr);
    } else {
      if (data) {
        return res
          .status(200)
          .json({ message: 'mail sent to expert', mailoption: info });
      }
    }
  });
};

var queryAdmin = function (req, res) {
  req.checkBody('studentId', 'Please send student Id');
  req.checkBody('query', 'Please send Query');
  studentModel.findById(req.body.studentId, function (err, data) {
    if (err) {
      throw err;
    } else {
      console.log('this is data', data);
    }
  });

  // nodemailer.createTestAccount((err,account)=>{
  //   let transporter = nodemailer.createTransport({
  //     host:'smtp.gmail.com',
  //     port : 587,
  //     secure:false,
  //     auth:{
  //       user:'sameer.sam9997@gmail.com',
  //       pass:'sameernida'
  //     }
  //   })
  //   var template =  "<p>Hello, Admin . A new Student has this query for you QUERY :-  has added a article <br/>  Name - {{name}}.</br> Email Id - {{email}}. </br> Created At "+
  //   "{{createdAt}}:</p> </br> <p>please Click <a href='http://159.65.145.191:3000/adminapproval/{{id}}'><u> Here </u> </a> to verify this very article</p>";
  //   var temp = Handlebars.compile(template);
  //   var context =  { name:expertdata.name ,email:expertdata.email,createdAt: expertdata.createdAt,id:articledata._id};
  //   var html = temp(context);
  //   let mailOptions = {
  //     from :'sameer.sam9997@gmail.com',
  //     to :'sameer.it78@gmail.com',
  //     subject:'Article For approval',
  //     text: 'Article Approval ',
  //     html: html
  //   }
  //   transporter.sendMail(mailOptions,(error,info)=>{
  //     if(error) {
  //       return console.log(error);
  //     }
  //     console.log("Message send %s",info.messageId);
  //     return res.status(200).json({message:'User Saved successfully ',mailoption:info});
  //     console.log("Preview URL %s",nodemailer.getTestMessageUrl(info));
  //   })
  // })
};

var editprofile = function (req, res) {
  req.checkParams('id', 'student id not received');
  req.checkBody('firstname', 'first name required');
  req.checkBody('class', 'class required');
  req.checkBody('age', 'Age required');
  console.log('these are languages', req.body.language);
  var profiledata = {
    firstname: req.body.firstname,
    class: req.body.class,
    age: req.body.age,
    languages: req.body.language,
    about: req.body.about ? req.body.about : ''
  };

  studentModel.findOneAndUpdate(
    { _id: req.params.id },
    profiledata,
    (err, data) => {
      if (err) {
        throw err;
      } else {
        console.log('this is the update data', data);
        res.json({ status: 200, message: 'data update', data });
      }
    }
  );
};

var getStudentsQueries = function (req, res) {
  queryModel
    .find({ isDeleted: false, selectedteacher: { $exists: false } })
    .populate('studentId', ['firstname', 'profileImage'])
    .exec((err, data) => {
      if (err) throw err;
      else {
        console.log('data population', data);
        res.json({
          status: 200,
          message: 'this is the the complete data',
          data
        });
      }
    });
};

var queryInsert = async function (req, res) {
  req.checkBody('title', 'title not reveived');
  req.checkBody('question', 'Question not received');
  req.checkBody('studentId', 'Student id not received');
  var queryObj = new Object();
  queryObj.title = req.body.title;
  queryObj.question = req.body.question;
  queryObj.studentId = req.body.studentId;
  // console.log("This is Query IMAGE ====================",req.body.queryImage);

  if (req.body.queryImage) {
    console.log('BASE 64', req.body.queryImage);
    var profile_base64Data = req.body.queryImage.replace(
      /^data:image\/jpeg;base64,/,
      ''
    );
    var complete_image_url =
      req.body.studentId + Math.floor(Math.random() * 600) + 1 + '.png';
    require('fs').writeFile(
      'public/queryimages/' + complete_image_url,
      profile_base64Data,
      'base64',
      function (gerr) {
        if (gerr) {
          console.log(
            'error while inserting the profile image for the first time ',
            gerr
          );
        }

        queryObj.queryImage = complete_image_url;
        queryModel(queryObj).save(function (err, data) {
          if (err) {
            throw err;
          } else {
            console.log('thisi s is data', data);
            res.json({ status: 200, message: 'Query created' });
          }
        });
      }
    );
  } else {
    queryModel(queryObj).save(function (err, data) {
      if (err) {
        throw err;
      } else {
        console.log('thisi s is data', data);
        res.json({ status: 200, message: 'Query created' });
      }
    });
  }
};

var uploadProfileImage = function (req, res) {
  req.checkParams('id', 'student id not received');
  req.checkBody('profileImage', 'profile image not received');
  var id = req.params.id;
  // console.log("this is the profile image",req.body.profileImage);
  var profile_base64Data = req.body.profileImage.replace(
    /^data:image\/png;base64,/,
    ''
  );

  studentModel.findById(id, (err, data) => {
    if (err) {
      res.send(err);
    } else {
      console.log('first phase data', data);
      if (data.profileImage) {
        console.log('another process started');
        require('fs').unlinkSync(
          'public/student/dp/' + data.profileImage,
          (derr, dat) => {
            if (derr) {
              console.log('This is the error ', derr);
            }
            console.log('image removed', dat);
          }
        );
        var complete_image_url =
          id + Math.floor(Math.random() * 600) + 1 + '.png';
        require('fs').writeFile(
          'public/student/dp/' + complete_image_url,
          profile_base64Data,
          'base64',
          function (gerr) {
            if (gerr) {
              console.log(
                'error while inserting the profile image for the first time '
              );
            }
            var dtdata = {
              profileImage: complete_image_url
            };
            studentModel.findByIdAndUpdate(
              id,
              dtdata,
              { new: true },
              (err, data) => {
                if (err) {
                  res.send(err);
                } else {
                  if (data) {
                    res.json({
                      status: 200,
                      message: 'teacher doc updated successfully',
                      data
                    });
                  }
                }
              }
            );
          }
        );
      } else {
        var complete_image_url =
          id + Math.floor(Math.random() * 500) + 1 + '.png';
        var ddata = {
          profileImage: complete_image_url
        };
        require('fs').writeFile(
          'public/student/dp/' + complete_image_url,
          profile_base64Data,
          'base64',
          function (gerr) {
            if (gerr) {
              console.log(
                'error while inserting the profile image for the first time '
              );
            }
            studentModel.findByIdAndUpdate(
              id,
              ddata,
              { new: true },
              (err, data) => {
                if (err) {
                  res.send(err);
                } else {
                  if (data) {
                    res.json({
                      status: 200,
                      message: 'Teacher docs updated successfully',
                      data
                    });
                  }
                }
              }
            );
          }
        );
      }
    }
  });
};

var removeStudentDp = function (req, res) {
  req.checkParams('id', 'student id not received');
  var id = req.params.id;
  // var profile_base64Data = req.body.profileImage.replace(/^data:image\/png;base64,/, "");
  studentModel.findById(id, (err, data) => {
    if (err) {
      res.send(err);
    } else {
      console.log('first phase data', data);
      if (data.profileImage) {
        console.log('another process started');
        require('fs').unlinkSync(
          'public/student/dp/' + data.profileImage,
          derr => {
            if (derr) {
              console.log('This is the error ', derr);
            }
          }
        );
        studentModel.findOneAndUpdate(
          { _id: id },
          { profileImage: '' },
          (err, data) => {
            if (err) throw err;
            else {
              console.log('this is the data after ***********', data);
              res.json({ status: 200, message: 'Profile image removed', data });
            }
          }
        );
      }
    }
  });
};

var logout = function (req, res) {
  req.checkParams('id', 'teacher id required');
  var id = req.params.id;
  studentModel.findOneAndUpdate({ _id: id }, { token: '' }, function (
    err,
    data
  ) {
    if (err) throw err;
    else {
      console.log('this is the data', data);
      res.json({ status: 200, message: 'successfully logged out' });
    }
  });
};

var askQueryToAdmin = function (req, res) {
  req.checkBody('title', 'Query Title Required');
  req.checkBody('question', 'Question required');
  req.checkBody('sender.senderId', 'sender id is required');
  console.log('thisi s the request', req.body.sender.senderId);
  var postJson = {
    title: req.body.title,
    question: req.body.question,
    sender: {
      senderId: req.body.sender.senderId,
      senderType: 'student'
    }
  };
  queryAdminModel(postJson).save(function (err, data) {
    if (err) throw err;
    else {
      console.log('data', data);
      res.json({ status: 200, message: 'this is the data', data });
    }
  });
};
var getSingleAdminQuery = function (req, res) {
  req.checkParams('id', 'Please send id');
  var id = req.params.id;
  queryAdminModel.findOne({ _id: id, isDeleted: false }, function (err, data) {
    if (err) throw err;
    console.log('this is data', data);
    res.json({ status: 200, message: 'getting all admin queries ', data });
  });
};

var getStudentAdminQueries = function (req, res) {
  req.checkParams('id', 'Please send student id ');
  var id = req.params.id;
  queryAdminModel
    .find({ 'sender.senderId': id })
    .sort('-createdAt')
    .exec(function (err, data) {
      if (err) throw err;
      else {
        if (data) {
          console.log('you got the data', data);
          res.json({ status: 200, message: 'you got all the queries', data });
        }
      }
    });
};

var studentAdminQueryReply = function (req, res) {
  req.checkParams('id', 'Please send the id');
  req.checkBody('userType', 'Please send usertype');
  req.checkBody('replyText', 'Please send reply text');
  req.checkBody('repliedAt', 'Please send reply time');
  var postData = {
    userType: req.body.userType,
    replyText: req.body.replyText,
    repliedAt: req.body.repliedAt
  };
  var id = req.params.id;
  queryAdminModel.findOneAndUpdate(
    { _id: id },
    { $push: { reply: postData } },
    function (err, data) {
      if (err) throw err;
      console.log('data updated', data);
      res.json({ status: 200, message: 'data updated', data });
    }
  );
};

var rateStudent = function (req, res) {
  req.checkParams('slug', 'Please send student id');
  req.checkBody('rating', 'Please send rating');
  req.checkBody('comment', 'Please send comment');
  console.log('this is body', req.body);
  // queryModel.findOne({slug:req.params.slug},{$in:"ratingandfeedback.comment"}).exec((err1,data)=>{
  //   if(err1) throw err1;
  //   console.log("you got the data",data);
  // })
  queryModel
    .findOneAndUpdate(
      { slug: req.params.slug },
      {
        'ratingandfeedback.comment': req.body.comment,
        'ratingandfeedback.rating': req.body.rating
      },
      { upsert: true }
    )
    .exec(function (err, data) {
      if (err) throw err;
      console.log('this is data', data);
      res.json({ status: 200, message: 'added the comment to query', data });
    });
};

var checkStudentEmail = function (req, res) {
  req.checkParams('email', 'Please send email to check');
  var email = req.params.email.trim();
  studentModel.findOne({ email: email, isDeleted: false }, function (err, data) {
    if (err) throw err;
    console.log('this is the data', data);
    if (data) {
      res.json({ status: 200, message: 'Email all-ready in use', data });
    }
  });
};

var messageToTeacher = function (req, res) {
  req.checkParams('slug', 'Please send query slug ');
  req.checkBody('userType', 'Please send usertype');
  req.checkBody('replyText', 'Please send reply text');
  req.checkBody('repliedAt', 'Please send reply time');

  var postData = {
    userType: req.body.userType,
    replyText: req.body.replyText,
    repliedAt: req.body.repliedAt
  };
  var slug = req.params.slug;
  queryModel.findOneAndUpdate(
    { slug: slug },
    { $push: { message: postData } },
    function (err, data) {
      if (err) throw err;
      console.log('data updated', data);
      res.json({ status: 200, message: 'data updated', data });
    }
  );
};

var joinSession = function (req, res) {
  // req.checkParams('id','send query ID to join a video session');
  // queryModel.findById(req.params.id, function(err,data) {
  //   if (err) throw err;
  //   const sessionId = data.sessionId;
  //   const options = {
  //     role : 'publisher',
  //     data :  'student',
  //   }
  //   const token_op = opentok.generateToken(sessionId, options);
  //   const newData = {...data._doc, token_op}
  //   res.send(newData)
  // })
};

var sessionWebhook = function (req, res) {
  //TODO
  //Sample response
  // {
  //   "sessionId": "2_MX40NjIzODI1Mn5-MTU0NTEzMDY5ODM2MH5KckowQU92SlhUZlZPT1gvTXVNTUdUSTN-fg",
  //   "projectId": "46238252",
  //   "event": "connectionDestroyed",
  //   "reason": "clientDisconnected",
  //   "timestamp": 1545130825529,
  //   "connection": {
  //     "id": "d5967a5d-5805-4f87-a32a-78b878b444a7",
  //     "createdAt": 1545130817805,
  //     "data": "student"
  //   }
  // }
  //if event connectioncreated and connection.data === student, add to db starttime
  //if event connectiondestroyed and connection.data === student, add to db endtime
  //then show endtime-starttime in minutes in frontend.
  //use socket to send notification to student on connectioncreated and data === teacher.
};

const addCard = async (req, res) => {
  //!client needs to send and identifier for user such as email or userId.
  // REPLACE REQ.BODY.STRIPEEMAIL by userEMAIL, this is for testing.
  console.log(req.body);
  req.checkBody('email', 'Please send email.');
  let customer;
  try {
    customer = await stripe.customers.create({
      source: "tok_amex",
      // email: req.body.stripeEmail // replace by user email
      email: req.body.email
    });
    console.log('costumer', customer)

  } catch (error) {
    return res.status(400).send({ error: error.message });
  }
  console.log('costumer', customer)
  studentModel.findOneAndUpdate(
    // { email: req.body.stripeEmail, isDeleted: false }, // replace by user email
    { email: req.body.email, isDeleted: false },
    { stripeId: customer.id },
    function (err, data) {
      if (err) {
        throw err;
      } else {
        res.send({ status: 200, message: data });
      }
    }
  );
};

const chargeCard = async (req, res) => {
  console.log('reqqqqqqqqqqqqqqqq', req.body)
  //!client needs to send and identifier for user such as email or userId.
  req.checkBody('email', 'Please send email.');
  let student;
  let charge;
  try {
    student = await studentModel.findOne({ email: req.body.email });
  } catch (error) {
    res.status(400).send({ error: error.message });
  }

  try {
    console.log('aaaaaaaaaaaaaaaa', student)
    // When it's time to charge the customer again, retrieve the customer ID.
    charge = await stripe.charges.create({
      amount: req.body.amount, // $15.00 this time
      currency: 'usd',
      customer: student.stripeId // Previously stored, then retrieved
    });
  } catch (error) {
    res.status(400).send({ error: error.message });
  }

  res.status(200).send({ charged: charge });
};

module.exports = {
  authenticate: authenticate,
  register: register,
  getstudentProfileData: getstudentProfileData,
  queryInsert: queryInsert,
  studentQueries: studentQueries,
  getsinglequery: getsinglequery,
  deleteStuQuery: deleteStuQuery,
  verifyEmail: verifyEmail,
  queryAdmin: queryAdmin,
  editprofile: editprofile,
  getStudentsQueries: getStudentsQueries,
  uploadProfileImage: uploadProfileImage,
  removeStudentDp: removeStudentDp,
  logout: logout,
  askQueryToAdmin: askQueryToAdmin,
  getSingleAdminQuery: getSingleAdminQuery,
  getStudentAdminQueries: getStudentAdminQueries,
  studentAdminQueryReply: studentAdminQueryReply,
  rateStudent: rateStudent,
  checkStudentEmail: checkStudentEmail,
  messageToTeacher: messageToTeacher,
  joinSession: joinSession,
  sessionWebhook: sessionWebhook,
  addCard: addCard,
  chargeCard: chargeCard
};
