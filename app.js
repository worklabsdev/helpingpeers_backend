require("dotenv").config();

appConfig = require(`./config/envs/${process.env.NODE_ENV}`);// App Config

express = require("express");

var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var bodyParser = require("body-parser");
var cors = require("cors");
var validator = require("express-validator");
var session = require("express-session");
const multipart = require("connect-multiparty");
const helmet = require("helmet");
const uuidv4 = require("uuid/v4");

// var userTeacherRouter = require("./routes/userTeacher");
var userStudentRouter = require("./routes/userStudent");
var userTeacherRouter = require("./routes/userTeacher");
var userAdminRouter = require("./routes/userAdmin");

app = express();

require("./packages");//i18 Other Necessary Packages
multipartMiddleware = multipart();

require("./config/db");

app.use(helmet());

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "hbs");

app.use(bodyParser.json({ limit: "500mb" }));
app.use(bodyParser.urlencoded({ limit: "500mb", extended: true, parameterLimit: 50000 }));
app.use(
  session({
    secret: appConfig.secretKey,
    resave: true,
    saveUninitialized: true
  })
);
app.use(logger("dev"));//   Logs Basics
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors({ origin: "*", optionsSuccessStatus: 200 }));
app.use(validator());
app.use(cookieParser());

app.use("/public/teacher/docs", express.static(__dirname + "/public/teacher/docs"));
app.use("/public", express.static(__dirname + "/public"));


app.use(function (req, res, next) {
  req.requestId = uuidv4();

  res["x-api-request-reference"] = req.requestId;
  next();
});

// app.use("/teacher", userTeacherRouter);

app.use("/stu", userStudentRouter);

app.use("/t", userTeacherRouter);
app.use("/ad", userAdminRouter);

require("./modules");// New Api's

const { pageNotFound, errorHandler } = require("./utils/responseHandler");

app.use((req, res, next) => {
  return pageNotFound(res);
});
// Error Handler
app.use((err, req, res, next) => {
  return errorHandler(res, err);
});

module.exports = app;
